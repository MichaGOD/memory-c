#include <stdio.h>
#include <math.h>

#pragma warning(disable:4996)


#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

void copyFile(FILE* sourceFile, FILE* destFile)
{
	char c = fgetc(sourceFile);
	while (c != EOF)
	{
		fputc(c, destFile);
		c = fgetc(sourceFile);
	}
}

/*
*	7f.	Call the function with the (<Name Of the Array>, 
								<Name Of the Array> + length / 2, 
								length / 2);
*/
void charArrSwap(char firstArr[], char secArr[], int length)
{
	for (int i = 0; i < length; i++)
	{
		char temp = firstArr[i];
		firstArr[i] = secArr[i];
		secArr[i] = temp;
	}
}

void charArrSetValues(char arr[], char initValue, int length)
{
	for (int i = 0; i < length; i++)
	{
		arr[i] = initValue;
	}
}

int charArrCompare(char firstArr[], char secondArr[], int length)
{
	for (int i = 0; i < length; i++)
	{
		if (firstArr[i] != secondArr[i])
			return 0;
	}
	return 1;
}

/*
	7d.	Answer: We just send the length argument with sizeof(<Arr Element Size>)
*/
void charArrCopy(char fromArr[], char toArr[], int length)
{
	for (int i = 0; i < length; i++)
	{
			toArr[i] = fromArr[i];
	}
}

typedef struct Rectangle {
	int length;
	int width;
} Rectangle;

void printRectangleArr(Rectangle recArr[], int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("Rectangle <%d>\n", i + 1);
		printf("Width=<%d>\n", recArr[i].width);
		printf("Height=<%d>\n", recArr[i].length);
		printf("\n");
	}
}

void printCharArr(char arr[], int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%c ", arr[i]);
	}
	printf("\n");
}

void printArr(int arr[], int length)
{
	for (int i = 0; i < length; i++)
	{
		printf("%d ", arr[i]);
	}
	printf("\n");
}

int fibValue(int index)
{
	if (index == 2 || index == 1) {
		return 1;
	}
	return fibValue(index - 1) + fibValue(index - 2);
}

void initializeFibArr(int arr[], int length)
{
	for (int i = 1; i <= length; i++)
	{
		arr[i - 1] = fibValue(i);
	}
}

void initializeRect(Rectangle* rect, int len, int wid)
{
	rect->width = wid;
	rect->length = len;
}

void swap(int* pNum1, int* pNum2)
{
	int temp = *pNum1;
	*pNum1 = *pNum2;
	*pNum2 = temp;
}

void add(int* pNum, int incBy) {
	*pNum += incBy;
}

int main() {
	int a[3] = {1, 2, 3};
	int b[3];
	printArr(b, 3);
	printf("\n");
	charArrCopy((char*) a, (char*) b, 3 * sizeof(int));
	printArr(b, 3);

	char c[4] = { 'a', 'b', 'c', 'd' };
	printf("Arr Char:\n");
	printCharArr(c, 4);
	charArrSwap(c, (c + 2), 2);
	printCharArr(c, 4);

	FILE* from = fopen("Ilan.txt", "r");
	FILE* to = fopen("Alon.txt", "w");
	copyFile(from, to);

	fclose(from);
	fclose(to);

	return 0;
}